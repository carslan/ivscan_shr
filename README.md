# IVscan_SHR

**Install ics Client**

It is necessary to install the _ics client for python_ to establish the connection to the SHR power supply.

To install the _icsClientPython_3_, simply copy the _icsClientPython_3_ folder into your site-packages folder. 
Example paths: '/home/USER/miniconda3/lib/python3.10/site-packages/', '/home/USER/.local/lib/python3.10/site-packages/' 

Then you have to change the path in the _IVscan_SHR.py_ file accordingly:

`icsClient = clientCommon.importFromPath("PATH/icsClientPython_3", "icsClientPython_3")`


**Settings in IVscan_SHR**

Things you want to check or change:


- Connection settings:
    - IP (connectionParameters)
    - Login/Passwort

- iv_curve_config 
