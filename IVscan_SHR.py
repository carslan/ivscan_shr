#import argparse
import time
#import importlib
import numpy as np
import logging
import matplotlib.pyplot as plt
from tqdm import tqdm
import os
import tables as tb
import clientCommon
import datetime

# Set up logging
logging.getLogger().setLevel(logging.INFO)

# import icsClient module
icsClient = clientCommon.importFromPath("/home/can/.local/lib/python3.10/site-packages/icsClientPython_3", "icsClientPython_3")
if icsClient == 0:
    print("Failed to import icsClient module => Exit.")
    exit(1)

# Get current date and time
now = datetime.datetime.now()
date_time = now.strftime("%Y-%m-%d_%H-%M-%S")

# Establish connection to power supply
connectionName = "new_iCS_connection"
connectionParameters = "ws://131.220.218.127"
connectionLogin = "admin"
connectionPassword = "testhvmaps"
connection = clientCommon.establishConnection(icsClient, connectionName, connectionParameters, connectionLogin, connectionPassword)
if type(connection) != icsClient.connection:
    logging.error("Failed to connect => Exit.")
    exit(1)

all_modules = connection.getModules()

# Set the IV curve configuration.
# The sign of the voltages of the iv_curve_config won't change the polarity of the power supply; no positive values are allowed.
# The polarity setting of the power supply gets checked before the scan.
iv_curve_config = {
    'channel': 0, # Channel of power supply: 0 or 1 for iSEG SHR
    'V_start': -115,  # Start value for bias voltage
    'V_stop': -125,  # Stop value for bias voltage
    'V_step': -0.5,  # Stepsize of bias voltage
    'V_final': -1,  # Return to V_final after measurement
    'V_max': -341,  # Max. value allowed for bias voltage
    'max_leakage': 10e-5,  # Max. leakage current allowed before aborting scan
    'wait_cycle': 0.5,  # Min. delay between current measurements
    'n_meas': 5,  # Number of current measurements per voltage step

    # Chip and output file info
    'chip_id': 'Run2020',
    'sensor_id': 'V5L2_programmed',
    'set_preamp': False,
}

# Validate voltage configuration values
values_to_check = ['V_start', 'V_stop', 'V_step', 'V_max', 'V_final'] 
for value in values_to_check:
    if iv_curve_config[value] >= 0:
        logging.error('ERROR: Voltages of IV curve config have to be negative to avoid damage to the sensor. Aborting scan.')
        exit(1)

# Define the data table structure
class DataTable(tb.IsDescription):
    voltage = tb.Float64Col(pos=0)
    current = tb.Float64Col(pos=1)
    current_err = tb.Float64Col(pos=2)
    n_meas = tb.Float64Col(pos=3)

class IV_Curve_Scan(object):
    scan_id = 'IV_curve'

    def __init__(self, set_preamp, chip_id, sensor_id, max_leakage, **_):
        # Create and open output file
        output_folder = os.path.join(os.getcwd(), "output_data/IV_curves")
        if set_preamp:
            scan_name = f"IVcurve_{chip_id}_{sensor_id}_preamp_{date_time}"
        else:
            scan_name = f"IVcurve_{chip_id}_{sensor_id}_{date_time}"
        self.output_filename = os.path.join(output_folder, scan_name)
        self.output_file = tb.open_file(self.output_filename + '.h5', mode='w', title=self.scan_id)
        self.data = self.output_file.create_table(self.output_file.root, name='IV_data', description=DataTable, title='IVcurve')

        #TODO: change the DUT to MightyPix
        '''
        # Initialize and power DUT (LF-Monopix2) to prevent applying HV to unpowered chip
        m = monopix2.Monopix2(no_power_reset=False)
        m.init()
        if set_preamp:
            m.set_preamp_en(pix='all', overwrite=True)
        time.sleep(5)
        '''


    def _scan(self, channel = 0, V_start=-0, V_stop=-10, V_step=-1, V_final=-10, V_max=-100, max_leakage=20e-6, wait_cycle=0.5, n_meas=5, **_):
        ''' Loop through voltage range and measure current at each step n_meas times'''
        if isinstance(V_step, int):
            set_voltages = list(range(V_start, V_stop, V_step))
        else:
            set_voltages = np.arange(V_start, V_stop, V_step) 
        logging.info(f'Measure IV for V from {set_voltages[0]} to {set_voltages[-1]}')    
        
        for modules in all_modules:
            time.sleep(1)
            polarity = modules.channels[channel].getSetupOutputPolarity()
            time.sleep(1)   
            logging.info(f'Polarity: {polarity}')
            if polarity != 'n':
                modules.channels[channel].setSetupOutputPolarity('n')
                logging.info(f'Polarity changed to n.')

            modules.channels[channel].setControlCurrentSet(1.15 * max_leakage)
            time.sleep(0.5)
            modules.channels[channel].setControlVoltageSet(V_start)
            time.sleep(0.5)
            v_meas = float(modules.channels[channel].getStatusVoltageMeasure())
            time.sleep(0.5)
            modules.channels[channel].setControlOn(1)
            while abs((abs(v_meas) - abs(V_start))) > 0.3:
                time.sleep(1)
                v_meas = float(modules.channels[channel].getStatusVoltageMeasure())
                logging.info('Waiting for power supply to ramp to V_start.')


            for voltage in tqdm(set_voltages, unit='Voltage step'):
                if voltage > 0:
                    RuntimeError('Voltage has to be negative! Abort to protect device.')

                if abs(voltage) <= abs(V_max):
                    modules.channels[channel].setControlVoltageSet(voltage)
                    time.sleep(wait_cycle * 5)
                    V_currently = voltage
                else:
                    logging.info(f'Maximum voltage with {voltage} V reached, abort')
                    break
                v_meas = float(modules.channels[channel].getStatusVoltageMeasure())
                #print(f'v_meas: {v_meas}')
                while (abs(abs(v_meas) - abs(voltage))) > 0.3:
                    curr = float(modules.channels[channel].getStatusCurrentMeasure())
                    if abs(curr) > abs(max_leakage):
                        logging.info(f'Maximum current with {curr} A reached, abort')
                        break 
                    time.sleep(1)
                    v_meas = float(modules.channels[channel].getStatusVoltageMeasure())
                    logging.info('Waiting for power supply to ramp.')
                    
                # Measure current
                currents = []
                try:
                    current = float(modules.channels[channel].getStatusCurrentMeasure())
                except Exception:
                    logging.warning('Could not measure current, skipping this voltage step!')
                    continue

                # Measure voltage
                measure_voltage = float(modules.channels[channel].getStatusVoltageMeasure())

                # Check leakage current limit
                if abs(current) > abs(max_leakage):
                    logging.info(f'Maximum current with {current} A reached, abort')
                    break
                    
                # Take mean over several measuerements
                for _ in range(n_meas):
                    current = float(modules.channels[channel].getStatusCurrentMeasure())
                    currents.append(current)
                    time.sleep(wait_cycle)


                # Store data
                #try:    
                #    sel = np.logical_and(np.array(currents) / np.mean(np.array(currents)) < 2.0, np.array(currents) / np.mean(np.array(currents)) > 0.5) 
                #except:
                #    sel = [ True,  True,  True,  True,  True]
                self.data.row['voltage'] = measure_voltage
                self.data.row['current'] = np.mean(np.array(currents))#[sel])
                self.data.row['current_err'] = np.std(currents)
                self.data.row['n_meas'] = n_meas
                self.data.row.append()
                self.data.flush()

            # Close output file
            self.output_file.close()


            # Ramp bias voltage down
            if V_currently != V_final:
                logging.info(f'Ramping bias voltage down from {V_currently} V to {V_final} V')
                for voltage in tqdm(np.arange(V_currently, V_final - V_step, -V_step*10)):
                    modules.channels[channel].setControlVoltageSet(voltage)
                    time.sleep(0.1)

            logging.info('Scan complete, turning off SMU')
            modules.channels[channel].setControlOn(0)

    def _plotlog(self, chip_id, sensor_id, **_):
        logging.info('Analyze and plot results')
        with tb.open_file(self.output_filename + '.h5', 'r+') as in_file_h5:
            data = in_file_h5.root.IV_data[:]
            
        x, y, yerr = data['voltage'] * (-1), data['current'] * (-1), data['current_err']
        plt.clf()
        plt.figure(figsize=(10,6))
        plt.errorbar(x, y, yerr, label='IV Data', marker = "o")
        plt.title(f'IV curve of {chip_id} (Sensor ID {sensor_id})')
        plt.yscale('log')
        plt.ylabel('Current / A')
        plt.xlabel('Voltage / V')
        plt.grid()
        plt.legend()
        plt.savefig(self.output_filename + '_log.pdf')

    def _plot(self, chip_id, sensor_id, **_):
        
        with tb.open_file(self.output_filename + '.h5', 'r+') as in_file_h5:
            data = in_file_h5.root.IV_data[:]
            
        x, y, yerr = data['voltage'] * (-1), data['current'] * (-1), data['current_err']
        plt.clf()
        plt.figure(figsize=(10,6))
        plt.errorbar(x, y, yerr, label='IV Data', marker = "o")
        plt.title(f'IV curve of {chip_id} (Sensor ID {sensor_id})')
        plt.ylabel('Current / A')
        plt.xlabel('Voltage / V')
        plt.grid()
        plt.legend()
        plt.savefig(self.output_filename + '.pdf')


if __name__ == '__main__':
    scan = IV_Curve_Scan(**iv_curve_config)
    scan._scan(**iv_curve_config)
    scan._plotlog(**iv_curve_config)
    scan._plot(**iv_curve_config)
